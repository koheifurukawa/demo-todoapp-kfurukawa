package com.example

import io.ktor.application.*
import io.ktor.response.*
import io.ktor.request.*
import io.ktor.routing.*
import io.ktor.http.*
import com.fasterxml.jackson.databind.*
import io.ktor.jackson.*
import io.ktor.features.*
import kotlin.test.*
import io.ktor.server.testing.*

import com.example.*
import org.junit.After

class ApplicationTest {

    /** ルートパスではTodoList!が出力される*/
    @Test
    fun testRoot() {
        withTestApplication(Application::module) {
            handleRequest(HttpMethod.Get, "/").apply {
                assertEquals(HttpStatusCode.OK, response.status())
                assertEquals("TodoList!", response.content)
            }
        }
    }

    /** ルートパスではTodoList!が出力される*/
    @Test
    fun testListRequest() {
        withTestApplication(Application::module) {
            handleRequest(HttpMethod.Get, "/todos").apply {
                assertEquals(HttpStatusCode.OK, response.status())
            }
        }
    }

    /** 登録が成功する場合*/
    @Test
    fun testCreateSuccessRequest(){
        val str: String = "{\"title\":\"勉強\",\"detail\":\"英語\",\"date\":\"2021-11-26\"}"
        withTestApplication(Application::module){
            handleRequest(HttpMethod.Post,"/todos"){
                addHeader(HttpHeaders.ContentType,ContentType.Application.Json.toString())
                setBody(str)
            }.run {
                assertEquals(HttpStatusCode.OK,response.status())
            }
        }
    }

    /** 登録が成功する場合(detailとdateが入力されていない場合)*/
    @Test
    fun testCreateSuccessRequestWithoutDetailAndDate(){
        val str: String = "{\"title\":\"勉強\"}"
        withTestApplication(Application::module){
            handleRequest(HttpMethod.Post,"/todos"){
                addHeader(HttpHeaders.ContentType,ContentType.Application.Json.toString())
                setBody(str)
            }.run {
                assertEquals(HttpStatusCode.OK,response.status())
            }
        }
    }

    /** 登録が失敗する場合(タイトルがない場合)*/
    @Test
    fun testCreateFailRequest() {
        val str: String = "{\"title\":\"\",\"detail\":\"英語\",\"date\":\"2021-11-26\"}"
        withTestApplication(Application::module){
            handleRequest(HttpMethod.Post,"/todos"){
                addHeader(HttpHeaders.ContentType,ContentType.Application.Json.toString())
                setBody(str)
            }.run {
                assertEquals(HttpStatusCode(500,"リクエストの形式が不正です"),response.status())
            }
        }
    }

    /** 更新が成功する場合*/
    @Test
    fun testUpdateSuccessRequest() {
        val strPut: String = "{\"title\":\"勉強\",\"detail\":\"沖縄\",\"date\":\"2021-12-12\"}"
        withTestApplication(Application::module){
            handleRequest(HttpMethod.Put,"/todos/1"){
                addHeader(HttpHeaders.ContentType,ContentType.Application.Json.toString())
                setBody(strPut)
            }.run {
                assertEquals(HttpStatusCode.OK,response.status())
            }
        }
    }
    /** 更新が失敗する場合(detailがない場合)*/
    @Test
    fun testUpdateSuccessRequestWithoutDetail() {
        val strPut: String = "{\"title\":\"勉強\",\"detail\":\"\",\"date\":\"2021-12-12\"}"
        withTestApplication(Application::module){
            handleRequest(HttpMethod.Put,"/todos/1"){
                addHeader(HttpHeaders.ContentType,ContentType.Application.Json.toString())
                setBody(strPut)
            }.run {
                assertEquals(HttpStatusCode(500,"リクエストの形式が不正です"),response.status())
            }
        }
    }

    /** 更新が失敗する場合(dateがない場合)*/
    @Test
    fun testUpdateSuccessRequestWithoutDate() {
        val strPut: String = "{\"title\":\"勉強\",\"detail\":\"数学\",\"date\":\"\"}"
        withTestApplication(Application::module){
            handleRequest(HttpMethod.Put,"/todos/1"){
                addHeader(HttpHeaders.ContentType,ContentType.Application.Json.toString())
                setBody(strPut)
            }.run {
                assertEquals(HttpStatusCode(500,"リクエストの形式が不正です"),response.status())
            }
        }
    }
    /** 更新が失敗する場合(titleがない場合)*/
    @Test
    fun testUpdateFailRequestWithoutTitle() {
        val strPut: String = "{\"title\":\"\",\"detail\":\"沖縄\",\"date\":\"2021-12-12\"}"
        withTestApplication(Application::module){
            handleRequest(HttpMethod.Put,"/todos/1"){
                addHeader(HttpHeaders.ContentType,ContentType.Application.Json.toString())
                setBody(strPut)
            }.run {
                assertEquals(HttpStatusCode(500,"リクエストの形式が不正です"),response.status())
            }
        }
    }
    /** 更新が失敗する場合(Detailがない場合)*/
    @Test
    fun testUpdateFailRequestWithoutDetail() {
        val strPut: String = "{\"title\":\"勉強\",\"detail\":\"\",\"date\":\"2021-12-12\"}"
        withTestApplication(Application::module){
            handleRequest(HttpMethod.Put,"/todos/1"){
                addHeader(HttpHeaders.ContentType,ContentType.Application.Json.toString())
                setBody(strPut)
            }.run {
                assertEquals(HttpStatusCode(500,"リクエストの形式が不正です"),response.status())
            }
        }
    }
    /** 更新が失敗する場合(dateがない場合)*/
    @Test
    fun testUpdateFailRequestWithoutDate() {
        val strPut: String = "{\"title\":\"勉強\",\"detail\":\"国語\",\"date\":\"\"}"
        withTestApplication(Application::module){
            handleRequest(HttpMethod.Put,"/todos/1"){
                addHeader(HttpHeaders.ContentType,ContentType.Application.Json.toString())
                setBody(strPut)
            }.run {
                assertEquals(HttpStatusCode(500,"リクエストの形式が不正です"),response.status())
            }
        }
    }

    /** 削除が成功する場合*/
    @Test
    fun testDeleteSuccessRequest() {
        withTestApplication(Application::module){
            handleRequest(HttpMethod.Delete,"/todos/15"){
                addHeader(HttpHeaders.ContentType,ContentType.Application.Json.toString())
            }.run {
                assertEquals(HttpStatusCode.OK,response.status())
            }
        }
    }

    /** 削除が失敗する場合*/
    @Test
    fun testDeleteFailRequest() {
        withTestApplication(Application::module){
            handleRequest(HttpMethod.Delete,"/todos/あいうえお"){
                addHeader(HttpHeaders.ContentType,ContentType.Application.Json.toString())
            }.run {
                assertEquals(HttpStatusCode(500,"リクエストの形式が不正です"),response.status())
            }
        }
    }

    /** サーバ内での不正なエラー(不正なURL)*/
    @Test
    fun testServerError() {
        withTestApplication(Application::module){
            handleRequest(HttpMethod.Delete,"/todos/test"){
                addHeader(HttpHeaders.ContentType,ContentType.Application.Json.toString())
            }.run {
                assertEquals(HttpStatusCode(500,"サーバ内で不明なエラーが発生しました"),response.status())
            }
        }
    }
}
