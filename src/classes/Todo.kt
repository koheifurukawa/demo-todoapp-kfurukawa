package com.example.classes

import org.joda.time.DateTime
import java.util.*
import javax.swing.plaf.basic.BasicInternalFrameTitlePane

// 扱いやすくする為にTodoデータクラス作成
data class Todo(
    val id: Int,
    val title: String,
    val detail: String?,
    val date: String?,
    val created_at: DateTime,
    val updated_at: DateTime,
)
/** 一覧表示のためのデータクラス*/
data class TodoList(
    val id:Int,
    val title: String,
    val detail: String?,
    val date: Date?,
)