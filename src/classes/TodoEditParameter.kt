package com.example.classes

import org.joda.time.DateTime
import java.io.ObjectInputStream
import java.text.SimpleDateFormat
import java.util.*

data class TodoEditParameter (
    var id: Int,
    val title: String,
    val detail: String?,
    val date: String?,
    val updated_at: DateTime?,
)