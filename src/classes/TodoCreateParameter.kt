package com.example.classes

import org.jetbrains.annotations.NotNull
import org.joda.time.DateTime
import java.io.ObjectInputStream
import java.text.SimpleDateFormat
import java.util.*

data class TodoCreateParameter (
    val title: String,
    val detail: String?,
    val date: String?,
    val created_at: DateTime?,
)