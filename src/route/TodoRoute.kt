package com.example.route

import com.example.TodoException.MyExceptionInfo
import com.example.classes.TodoCreateParameter
import com.example.classes.TodoEditParameter
import com.example.classes.TodoList
import com.example.repository.TodoRepository
import com.example.validation.Validation
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import java.text.SimpleDateFormat

    fun Route.todos() {
        val todoRepository = TodoRepository()

        route("/todos"){
            /** 一覧出力処理*/
            get {
                val todoSb = StringBuilder()
                val listErrorMessage = MyExceptionInfo()
                try {
                    var todoList: List<TodoList> = todoRepository.findAll()
                    var i = 0
                    /** 出力文字列形成*/
                    todoSb.append(" \"todos:\"[")
                    while (i < todoList.size) {
                        todoSb.append("\n    {")
                        todoSb.append("\n\t \"id\":${todoList.get(i).id}")
                        todoSb.append("\n\t \"title\":${todoList.get(i).title}")
                        if(todoList.get(i).detail == null) {
                            todoSb.append("\n\t \"detail\":\"\"")
                        }else{
                            todoSb.append("\n\t \"detail\":${todoList.get(i).detail}")

                        }
                        if(todoList.get(i).date == null) {
                            todoSb.append("\n\t \"date\":\"\"")
                        }else if(todoList.get(i).date != null){
                            val df = SimpleDateFormat("yyyy-MM-dd")
                            var str = df.format(todoList.get(i).date)
                            todoSb.append("\n\t \"date\":$str")
                        }
                        todoSb.append("\n    }")
                        i++
                    }
                    todoSb.append("\n  ]\n}")
                }catch (e: Exception){
                    listErrorMessage.error_code = 3
                    listErrorMessage.error_message = e.toString()
                }
                if(listErrorMessage.error_code == 0) {
                    todoSb.insert(0,"{\n  \"error_code\":${listErrorMessage.error_code},\n  \"error_message\":\"\",\n ")
                    call.respondText(todoSb.toString(), contentType = ContentType.Text.Plain, status = HttpStatusCode.OK)
                }else {
                    if (listErrorMessage.error_code != 0) {
                        var todoErrorSb = StringBuilder()
                        todoErrorSb.insert(0,"{\n  \"error_code\":${listErrorMessage.error_code},\n  \"error_message\":\"${listErrorMessage.error_message}\",\n}")
                        call.respondText(todoErrorSb.toString(), contentType = ContentType.Text.Plain, status = HttpStatusCode(500,"一覧の取得に失敗しました"))

                    }
                }


            }
            /** 登録処理*/
            post {
                val todoSb = StringBuilder()
                val listErrorMessage = MyExceptionInfo()
                try {
                    val parameter = call.receive<TodoCreateParameter>()
                    val vd = Validation()
                    /** バリデーションチェック*/
                    if(vd.validationCheckAtCreate(parameter.title).isNotEmpty()) {
                        vd.validationCheckAtCreate(parameter.title).forEach {
                            listErrorMessage.error_code = it.key
                            listErrorMessage.error_message = it.value
                        }
                    }
                    val todoRepository = TodoRepository()
                    todoRepository.create(parameter)
                } catch (e: Exception) {
                    listErrorMessage.error_code = 4
                    listErrorMessage.error_message = e.toString()
                }
                if (listErrorMessage.error_code == 0) {
                    todoSb.insert(0, "{\n  \"error_code\":${listErrorMessage.error_code},\n  \"error_message\":\"\",\n}")
                    call.respondText(todoSb.toString(), contentType = ContentType.Text.Plain, status = HttpStatusCode.OK)
                } else {
                    if (listErrorMessage.error_code != 0) {
                        var todoErrorSb = StringBuilder()
                        todoErrorSb.insert(
                            0,
                            "{\n  \"error_code\":${listErrorMessage.error_code},\n  \"error_message\":\"${listErrorMessage.error_message}\",\n}"
                        )
                        when(listErrorMessage.error_code) {
                            4 ->call.respondText(
                                todoErrorSb.toString(),
                                contentType = ContentType.Text.Plain,
                                status = HttpStatusCode(500, listErrorMessage.error_message)
                            )
                            2->call.respondText(
                                todoErrorSb.toString(),
                                contentType = ContentType.Text.Plain,
                                status = HttpStatusCode(500, listErrorMessage.error_message)
                            )
                        }
                    }

                }

            }
            /** 更新処理*/
            put ("/{id}"){
                val todoSb = StringBuilder()
                val listErrorMessage = MyExceptionInfo()
                try {
                    val id = call.parameters["id"]?.toInt()
                    val parameter = call.receive<TodoEditParameter>()
                    id?.let { parameter.id = it }
                    val vd = Validation()
                    /** タイトルのバリデーションチェック*/
                    if(vd.validationCheckAtEdit(id.toString(),parameter.title).isNotEmpty()) {
                        vd.validationCheckAtEdit(id.toString(),parameter.title).forEach {
                            listErrorMessage.error_code = it.key
                            listErrorMessage.error_message = it.value
                        }
                    }
                    /** 詳細のバリデーションチェック*/
                    if(vd.validationCheckAtEditDetail(parameter.detail.toString()).isNotEmpty()){
                        listErrorMessage.error_code = 2
                        listErrorMessage.error_message = vd.validationCheckAtEditDetail(parameter.detail.toString()).getOrDefault(2,"リクエストの形式が不正です")
                    }
                    /** 日付のバリデーションチェック*/
                    if(vd.validationCheckAtEditDate(parameter.date.toString()).isNotEmpty()){
                        listErrorMessage.error_code = 2
                        listErrorMessage.error_message = vd.validationCheckAtEditDetail(parameter.date.toString()).getOrDefault(2,"リクエストの形式が不正です")
                    }
                    val todoRepository = TodoRepository()
                    todoRepository.edit(parameter)
                } catch (e: Exception) {
                    listErrorMessage.error_code = 5
                    listErrorMessage.error_message = e.toString()
                }
                if (listErrorMessage.error_code == 0) {
                    todoSb.insert(0, "{\n  \"error_code\":${listErrorMessage.error_code},\n  \"error_message\":\"\",\n}")
                    call.respondText(todoSb.toString(), contentType = ContentType.Text.Plain, status = HttpStatusCode.OK)
                } else {
                    if (listErrorMessage.error_code != 0) {
                        var todoErrorSb = StringBuilder()
                        todoErrorSb.insert(
                            0,
                            "{\n  \"error_code\":${listErrorMessage.error_code},\n  \"error_message\":\"${listErrorMessage.error_message}\",\n}"
                        )
                        when(listErrorMessage.error_code) {
                            5 ->call.respondText(
                                todoErrorSb.toString(),
                                contentType = ContentType.Text.Plain,
                                status = HttpStatusCode(500, listErrorMessage.error_message)
                            )
                            2->call.respondText(
                                todoErrorSb.toString(),
                                contentType = ContentType.Text.Plain,
                                status = HttpStatusCode(500, listErrorMessage.error_message)
                            )
                        }
                    }
                }
            }
            /**削除処理*/
            delete("/{id}") {
                val todoSb = StringBuilder()
                val listErrorMessage = MyExceptionInfo()
                try {
                    val vd: Validation = Validation()
                    /** バリデーションチェック*/
                    if (vd.validationCheckAtDelete(call.parameters["id"]).get(2) == "リクエストの形式が不正です"){
                        listErrorMessage.error_code = 6
                        listErrorMessage.error_message = "削除に失敗しました"
                    }else {
                        val id = call.parameters["id"]?.toInt() ?: return@delete
                        val todoRepository = TodoRepository()
                        todoRepository.delete(id)
                    }
                } catch (e: Exception) {
                    listErrorMessage.error_code = 6
                    listErrorMessage.error_message = e.toString()
                }
                if (listErrorMessage.error_code == 0) {
                    todoSb.append("{\n  \"error_code\":${listErrorMessage.error_code},\n  \"error_message\":\"\",\n}")
                    call.respondText(todoSb.toString(), contentType = ContentType.Text.Plain, status = HttpStatusCode.OK)
                } else {
                    if (listErrorMessage.error_code != 0) {
                        var todoErrorSb = StringBuilder()
                        todoErrorSb.append(
                            "{\n  \"error_code\":${listErrorMessage.error_code},\n  \"error_message\":\"${listErrorMessage.error_message}\",\n}"
                        )
                        call.respondText(todoErrorSb.toString(), contentType = ContentType.Text.Plain, status = HttpStatusCode(500,"削除に失敗しました"))
                    }
                }

            }
        }
    }