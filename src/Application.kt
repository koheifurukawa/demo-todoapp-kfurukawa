package com.example

import com.example.classes.TodoList
import com.example.repository.TodoRepository
import io.ktor.application.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.http.*
import com.fasterxml.jackson.databind.*
import io.ktor.jackson.*
import io.ktor.features.*
import org.jetbrains.exposed.sql.*
import com.example.TodoException.MyExceptionInfo
import com.example.classes.TodoCreateParameter
import com.example.classes.TodoEditParameter
import com.example.route.todos
import com.example.validation.Validation
import io.ktor.request.*
import java.rmi.ServerException
import java.text.SimpleDateFormat

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)
@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false){
    val listErrorMessage = MyExceptionInfo()
    try {
        install(ContentNegotiation) {
            jackson {
                enable(SerializationFeature.INDENT_OUTPUT)
            }
        }

        routing {

            get("/") {
                call.respondText("TodoList!", contentType = ContentType.Text.Plain)
            }
            todos()
        }

        connectDB()
    }catch(e: ServerException ){
        val listErrorMessage = MyExceptionInfo()
        listErrorMessage.error_code = 1
        print("{\n  \"error_code\":${listErrorMessage.error_code},\n  \"error_message\":\"${listErrorMessage.error_message}\",\n}")
    }
    if (listErrorMessage.error_code != 0) {
        print("{\n  \"error_code\":${listErrorMessage.error_code},\n  \"error_message\":\"${listErrorMessage.error_message}\",\n}")
    }

}

private fun connectDB() {
    Database.connect(
        url = "jdbc:mysql://127.0.0.1/todo",
        driver = "com.mysql.cj.jdbc.Driver",
        user = "root",
        password = "password"
    )
}
