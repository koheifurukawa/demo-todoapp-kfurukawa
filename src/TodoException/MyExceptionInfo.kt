package com.example.TodoException

import java.rmi.ServerException

open class MyExceptionInfo{
    var error_code:Int = 0
    get():Int {
        return field
    }
    set(value){
        field =value
    }
    var error_message:String = ""
    get():String {
        return field
    }
    set(value) {
        field = value
    }

}