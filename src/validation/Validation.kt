package com.example.validation

class Validation {
    fun validationCheckAtCreate(title: String): Map<Int,String>{
        /** titleの入力値がない場合*/
        var map: Map<Int,String> = hashMapOf()
        if(title.isEmpty()){
            map = mapOf(2 to "リクエストの形式が不正です")
        }
        /** 入力文字数がvarchar[100]を超える場合*/
        if(title.length > 33){
            map = mapOf(2 to "リクエストの形式が不正です")
        }
        return map
    }

    fun validationCheckAtEdit(id: String,title: String): Map<Int,String> {
        /** idの入力値がない場合*/
        var map: Map<Int, String> = hashMapOf()
        if (id.isEmpty()) {
            map = mapOf(2 to "リクエストの形式が不正です")
        }
        /** idが0の場合*/
        if(id == "0"){
            map = mapOf(2 to "リクエストの形式が不正です")
        }
        /** titleの入力値がない場合(空文字)*/
        if (title.isEmpty()) {
            map = mapOf(2 to "リクエストの形式が不正です")
        }
        /** titleの文字数がvarchar[100]を超える場合*/
        if (title.length > 33) {
            map = mapOf(2 to "リクエストの形式が不正です")
        }
        return map
    }
    fun validationCheckAtEditDetail(detail: String): Map<Int,String> {
        var map: Map<Int, String> = hashMapOf()
        /** detailの入力値がない場合(空文字)*/
        if (detail.isEmpty()) {
            map = mapOf(2 to "リクエストの形式が不正です")
        }
        /** detailの文字数がvarchar[1000]を超える場合*/
        if (detail.length > 333) {
            map = mapOf(2 to "リクエストの形式が不正です")
        }
        return map
    }
    fun validationCheckAtEditDate(date: String): Map<Int,String> {
        var map: Map<Int, String> = hashMapOf()
        /** dateの入力値がない場合(空文字)*/
        if (date.isEmpty()) {
            map = mapOf(2 to "リクエストの形式が不正です")
        }
        return map
    }
    fun validationCheckAtDelete(id: String?): Map<Int,String>{
        var bool: Boolean = false
        var map: Map<Int, String> = hashMapOf()
        /** 受け取ったidがnullの場合*/
        if (id == null){
            return mapOf(2 to "リクエストの形式が不正です")
        }
        /** 受け取ったidが数値以外の場合*/
        try{
            var int: Int = 0
            int = Integer.parseInt(id)
        }catch (e: Exception){
            bool = true
        }
        when(bool){
            true -> map = mapOf(2 to "リクエストの形式が不正です")
            false -> map = mapOf(0 to "")
        }
        return map
    }
}