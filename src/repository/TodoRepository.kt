package com.example.repository

import com.example.classes.*
import com.example.db.table.TodoTables
import io.ktor.util.*
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.*
import org.joda.time.DateTime

class TodoRepository {
    fun findAll(): List<TodoList> {/** TodoList型のリストを返す*/
        return transaction {
                TodoTables.Todos.selectAll().sortedBy { it[TodoTables.Todos.date] }.map {
                    /** DBのtodosを受け取る*/
                    TodoList(
                        /** 形式を整える*/
                        id = it[TodoTables.Todos.id].value,
                        title = it[TodoTables.Todos.title].toString(),
                        detail = it[TodoTables.Todos.detail]?.toString(),
                        date = it[TodoTables.Todos.date]?.toDate(),
                    )
                }
        }
    }
    /** insert into */
    fun create(parameter: TodoCreateParameter) {
        transaction {
            TodoTables.Todos.insert {
                /** タイトルは必須*/
                it[title] = parameter.title
                /** 詳細が入力されない場合は追加しない*/
                if(parameter.detail != null) {
                    it[detail] = parameter.detail.toString()
                }
                /** 日付が入力されない場合は追加しない*/
                if(parameter.date != null) {
                    val dateTime: DateTime = DateTime.parse(parameter.date)
                    it[date] = dateTime
                }
                it[created_at] = DateTime.now()
            }
        }
    }

    /** update */
    fun edit(parameter: TodoEditParameter) {
        transaction {
            TodoTables.Todos.update(where = { TodoTables.Todos.id eq parameter.id }) {
                /** タイトルは必須*/
                it[title] = parameter.title
                /** 詳細が入力されている場合には更新する*/
                if(parameter.detail == null){
                    it[detail] = "\"\""
                }else{
                    it[detail] = parameter.detail.toString()
                }
                /** 日付が入力されている場合には更新する*/
                if(parameter.date != null) {
                    val dateTime: DateTime = DateTime.parse(parameter.date)
                    it[date] = dateTime
                }
                it[updated_at] = DateTime.now()
            }
        }
    }
    /** delete */
    fun delete(id: Int) {
        transaction {
            TodoTables.Todos.deleteWhere {
                /** 入力されたidに紐づく行を削除する*/
                TodoTables.Todos.id eq id
            }
        }
    }
}